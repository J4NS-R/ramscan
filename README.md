# RamScan

A set of [Volatility](https://github.com/volatilityfoundation/volatility) plugins for performing password forensics on Linux memory images.

Watch an in-depth [Demo Video here](https://siasky.net/CADihIhO88mR4P_UwrmhUoNdqesSjxxFx6YWikbiR2dfLQ)!

## Installation

Just copy the plugin `.py` files into the volatility plugin directory. [Details in the wiki](https://gitlab.com/koellewe/ramscan/-/wikis/Installation).

## Usage

General and per-plugin documentation can be found [in the wiki](https://gitlab.com/koellewe/ramscan/-/wikis/General-usage). 
