
"""Volatility plugin to scan applications for in-memory passwords"""

import volatility.plugins.linux.common as common
import volatility.plugins.linux.pslist as pslist
import volatility.plugins.linux.dump_map as dump_map
import volatility.debug as debug
from zipfile import ZipFile
from PyPDF2 import PdfFileReader

from volatility.renderers import TreeGrid

SUPPORTED_APPS = ['engrampa', 'atril',]
MIN_PWD_LEN = 4


def extract_strings(dumper, task, vma):
    """Gets all the ascii strings in the given vma, and maps them by occurrences"""
    string_occs = {}
    for page in dumper.read_addr_range(task, vma.vm_start, vma.vm_end):
        tmp_text = ''
        for unit in page:
            ordinal = ord(unit)
            if 32 <= ordinal <= 126:  # ascii text
                tmp_text += unit
            else:  # null or something
                if len(tmp_text) > 0:  # store string being built
                    if len(tmp_text) >= MIN_PWD_LEN:
                        if tmp_text in string_occs:
                            string_occs[tmp_text] += 1
                        else:
                            string_occs[tmp_text] = 1
                    tmp_text = ''
    return string_occs


class linux_app_pass(common.AbstractLinuxCommand):
    """Searches through app memory for passwords, and tests them against given files.

Use the -t flag to specify a comma-separated list of encrypted files to test extracted
passwords against. More details available at https://gitlab.com/koellewe/ramscan/-/wikis/Linux_App_Pass"""

    def __init__(self, config, *args, **kwargs):
        super(linux_app_pass, self).__init__(config, *args, **kwargs)
        config.add_option("TEST_FILES", short_option='t', default=None,
                          help='Encrypted files to test passwords against (comma-separated)',
                          action='store', type='str')

    def validate(self):
        if self._config.TEST_FILES is None:
            debug.error('Please specify a comma-separated list of files to test passwords for (--test-files)')

    def calculate(self):

        proc_list = pslist.linux_pslist(self._config).calculate()

        for task in proc_list:
            if SUPPORTED_APPS.count(task.comm) == 0:
                continue  # only interested in supported tasks

            # set config options for dumper
            self._config.PID = str(task.pid)
            # dump!
            dumper = dump_map.linux_dump_map(self._config)
            map_data = dumper.calculate()

            # get relevant memory slice
            virt_addrs = []
            for _, vma in map_data:
                virt_addrs.append(vma)
            virt_addrs.sort(key=lambda vma: vma.vm_start)
            vma = virt_addrs[5]  # always the 6th

            # extract strings from the vma
            string_occs = extract_strings(dumper, task, vma)

            # search through pages
            if task.comm == 'engrampa':

                # weed out - password occurs thrice, one of which has "-p" prepended
                candidates = []
                for string, occs in string_occs.items():
                    if string.startswith('-p') and occs == 1:
                        core = string[2:]  # min len 4
                        if core in string_occs and string_occs[core] == 2:
                            candidates.append(core)

                # test candidates
                for testfile in self._config.TEST_FILES.split(','):
                    if testfile.endswith('.zip'):  # crude, I know
                        zf = ZipFile(testfile, 'r')
                        internal_file = zf.namelist()[0]
                        for candidate in candidates:
                            try:
                                f = zf.open(internal_file, 'r', pwd=candidate)
                            except RuntimeError:
                                pass
                            else:
                                f.close()
                                yield task, testfile, candidate
                        zf.close()

            elif task.comm == 'atril':

                # weed out - password occurs twice
                candidates = []
                for string, occs in string_occs.items():
                    if occs == 2:
                        candidates.append(string)

                # test candidates
                for testfile in self._config.TEST_FILES.split(','):
                    if testfile.endswith('.pdf'):  # crude, I know
                        pf = open(testfile, 'rb')
                        pdf_reader = PdfFileReader(pf)

                        for candidate in candidates:
                            if pdf_reader.decrypt(candidate) > 0:
                                yield task, testfile, candidate
                                break
                        pf.close()

    def unified_output(self, data):
        """Standardised output"""
        return TreeGrid([("Time", str),
                         ("Program", str),
                         ("File", str),
                         ("Password", str)],
                        self.generator(data))

    def generator(self, data):
        """Convert data to unified output format"""
        for task, testfile, passwd in data:
            yield 0, [
                str(task.get_task_start_time()),
                task.comm,
                testfile,
                passwd
            ]

    def render_text(self, outfd, data):
        # this function runs before calculate() (since it's a generator)
        self.validate()

        self.table_header(outfd, [
            ('Time', '32'),
            ('Program', '8'),
            ('File', '32'),
            ('Password', '32')
        ])

        for task, testfile, passwd in data:
            self.table_row(outfd,
                           str(task.get_task_start_time()),
                           task.comm,
                           testfile,
                           passwd)


